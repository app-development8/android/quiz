# Quiz App

A simple Quiz App with multiple Screens and activities.

User can put in his username and go through the various questions as seen in the pictures.

<table>
    <tr>
        <td>
            <img src="https://gitlab.com/app-development8/android/quiz/-/raw/main/app/src/main/res/drawable/demo_start_screen.png"       width="300" height="585"/>
        </td>
        <td>
            <img src="https://gitlab.com/app-development8/android/quiz/-/raw/main/app/src/main/res/drawable/demo_question_screen.png"       width="300" height="585"/>
        </td>
    </tr>
    <tr>
        <td>
            <img src="https://gitlab.com/app-development8/android/quiz/-/raw/main/app/src/main/res/drawable/demo_finish_screen.png"       width="300" height="585"/>
        </td>
        <td></td>
    </tr>
</table>



