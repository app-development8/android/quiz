package de.peralty.quiz

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import de.peralty.quiz.data.Constants

class ResultActivity : AppCompatActivity() {

    private var mUserName: String? = null
    private var mCorrectAnswers: Int = 0
    private var mTotalQuestions: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_results)

        mUserName = intent.getStringExtra(Constants.USER_NAME)
        mCorrectAnswers = intent.getIntExtra(Constants.CORRECT_ANSWERS,0)
        mTotalQuestions = intent.getIntExtra(Constants.TOTAL_QUESTIONS,0)

        val tvName: TextView = findViewById(R.id.tv_name)
        val tvScore: TextView = findViewById(R.id.tv_score)
        val btnFinish: Button = findViewById(R.id.btn_finish)

        tvName.setText(mUserName)
        tvScore.setText("Your Score is $mCorrectAnswers out of $mTotalQuestions")

        btnFinish.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }
}